package org.jobinterview.task.configuration;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Getter
@Setter
@Component
@ConfigurationProperties("path.to")
public class PhoneBookConfigurationProperties {

    private String file;

}
