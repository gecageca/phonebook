package org.jobinterview.task.configuration;

import org.jobinterview.task.model.PhoneBook;
import org.jobinterview.task.service.PhoneBookService;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class StartUpInit {

    private PhoneBookService phoneBookService;

    StartUpInit(PhoneBookService phoneBookService) {
        this.phoneBookService = phoneBookService;
    }

    @PostConstruct
    public void init() throws IOException {
        phoneBookService.readTextFileAndWriteToDB();
        initialDataWithPhoneCallsForTestingPurpose();
    }


    private void initialDataWithPhoneCallsForTestingPurpose() {

        List<PhoneBook> list = new ArrayList<>();

        PhoneBook phoneBookTestData = new PhoneBook("test", "+359883466291");
        phoneBookTestData.setPhoneCallsCounter(2);
        list.add(phoneBookTestData);

        PhoneBook phoneBookTestData1 = new PhoneBook("test1", "+359883477391");
        phoneBookTestData1.setPhoneCallsCounter(10);
        list.add(phoneBookTestData1);

        PhoneBook phoneBookTestData2 = new PhoneBook("test2", "+359883477392");
        phoneBookTestData2.setPhoneCallsCounter(40);
        list.add(phoneBookTestData2);

        PhoneBook phoneBookTestData3 = new PhoneBook("test3", "+359883477393");
        phoneBookTestData3.setPhoneCallsCounter(25);
        list.add(phoneBookTestData3);

        PhoneBook phoneBookTestData4 = new PhoneBook("test4", "+359883477394");
        phoneBookTestData4.setPhoneCallsCounter(50);
        list.add(phoneBookTestData4);

        PhoneBook phoneBookTestData5 = new PhoneBook("test5", "+359883477395");
        phoneBookTestData5.setPhoneCallsCounter(2);
        list.add(phoneBookTestData5);

        PhoneBook phoneBookTestData6 = new PhoneBook("test6", "+359883477396");
        phoneBookTestData6.setPhoneCallsCounter(3);
        list.add(phoneBookTestData6);

        PhoneBook phoneBookTestData7 = new PhoneBook("test7", "+359883466297");
        phoneBookTestData7.setPhoneCallsCounter(4);
        list.add(phoneBookTestData7);

        PhoneBook phoneBookTestData8 = new PhoneBook("test8", "+359883466298");
        phoneBookTestData8.setPhoneCallsCounter(2);
        list.add(phoneBookTestData8);

        PhoneBook phoneBookTestData9 = new PhoneBook("test9", "+359883466299");
        phoneBookTestData9.setPhoneCallsCounter(2);
        list.add(phoneBookTestData9);

        PhoneBook phoneBookTestData10 = new PhoneBook("test10", "+359883466291");
        phoneBookTestData10.setPhoneCallsCounter(2);
        list.add(phoneBookTestData10);

        phoneBookService.saveAll(list);
    }
}