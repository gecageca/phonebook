package org.jobinterview.task.model;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PhoneBookDTO extends PhoneBook {

    private Integer phoneCallsCounter;

    public PhoneBookDTO(Long id, String name, String phoneNumber, Integer phoneCallsCounter) {
        super(id, name, phoneNumber);
        this.phoneCallsCounter = phoneCallsCounter;

    }
}
