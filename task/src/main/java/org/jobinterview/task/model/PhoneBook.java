package org.jobinterview.task.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class PhoneBook {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;


    @NotBlank
    @Column(unique = true)
    @Length(max = 255)
    private String name;

    @NotBlank
    @Length(max = 14)
    private String phoneNumber;

//    @ElementCollection
//    @CollectionTable(
//            name = "PHONE_BOOK_PHONE_NUMBER_LIST",
//            joinColumns=@JoinColumn(name = "id", referencedColumnName = "id")
//    )
//    private List<String> phoneNumberList;

    @JsonIgnore
    private Integer phoneCallsCounter;


    public PhoneBook(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.phoneCallsCounter = 0;
    }

    public PhoneBook(Long id, String name, String phoneNumber) {
        this.id = id;
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

//    public PhoneBook(String name, List<String> phoneNumber) {
//        this.name = name;
//        this.phoneNumberList = phoneNumber;
//        this.phoneCallsCounter = 0;
//    }


}
