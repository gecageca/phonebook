package org.jobinterview.task.service;

import lombok.extern.slf4j.Slf4j;
import org.jobinterview.task.configuration.PhoneBookConfigurationProperties;
import org.jobinterview.task.exception.PhoneBookNotFoundException;
import org.jobinterview.task.model.PhoneBook;
import org.jobinterview.task.model.PhoneBookDTO;
import org.jobinterview.task.repository.PhoneBookRepository;
import org.jobinterview.task.util.BatchUtil;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class PhoneBookService {

    private static final Charset ENCODING = StandardCharsets.UTF_8;
    private static final String PHONE_NUMBER_REGEX = "^.{0}([+3598]{5})([7-9]{1})([2-9]{1})([0-9]{6})";
    private static final String PHONE_NUMBER_REGEX_CODE = "08|003598";
    private static final String PHONE_NUMBER_REGEX_CODE_REPLACEMENT = "+3598";
    private static final Integer BATCH_SIZE = 100;

    private static final String QUERY = "select new org.jobinterview.task.model.PhoneBookDTO" +
            "(pb.id, pb.name, pb.phoneNumber, pb.phoneCallsCounter ) " +
            "FROM PhoneBook as pb  ORDER BY pb.phoneCallsCounter DESC ";

    private PhoneBookRepository phoneBookRepository;
    private PhoneBookConfigurationProperties phoneBookConfigurationProperties;
    private EntityManager entityManager;


    public PhoneBookService(PhoneBookRepository phoneBookRepository,
                            PhoneBookConfigurationProperties phoneBookConfigurationProperties,
                            EntityManager entityManager) {
        this.phoneBookRepository = phoneBookRepository;
        this.phoneBookConfigurationProperties = phoneBookConfigurationProperties;
        this.entityManager = entityManager;

    }


    public PhoneBook addContact(PhoneBook phoneBook) {
        log.info("Adding phone book");

        PhoneBook phoneBookFromDB = phoneBookRepository.findByName(phoneBook.getName());
        if (Objects.nonNull(phoneBookFromDB)) {
            throw new IllegalArgumentException("A contact with that name already exist");
        }

        phoneBook.setPhoneNumber(this.normalizePhoneNumber(phoneBook.getPhoneNumber()));
        if (!this.validatePhoneNumber(phoneBook.getPhoneNumber())) {
            throw new IllegalArgumentException("Please enter a valid phone number");
        }

        phoneBook.setPhoneCallsCounter(0);
        return phoneBookRepository.save(phoneBook);
    }

    public void deleteContactByName(String name) {
        PhoneBook phoneBook = phoneBookRepository.findByName(name);

        if (Objects.isNull(phoneBook)) {
            throw new PhoneBookNotFoundException("Phone book for given name is not found");
        }
        phoneBookRepository.delete(phoneBook);
        log.info("Successfully deleted phone book");
    }

    public PhoneBook findByName(String name) {
        return phoneBookRepository.findByName(name);
    }

    public Page<PhoneBook> findAllOrderedByName(Integer page, Integer size) {
        return phoneBookRepository.findAll(PageRequest.of(page, size, Sort.Direction.ASC, "name"));
    }

    public void makeAphoneCall(String phoneCall) {
        PhoneBook phoneBook = phoneBookRepository.findByPhoneNumber(phoneCall)
                .orElseThrow(() -> new PhoneBookNotFoundException("Phone book for given phone number is not found"));

        phoneBook.setPhoneCallsCounter(phoneBook.getPhoneCallsCounter() + 1);
        phoneBookRepository.save(phoneBook);
    }

    public List<PhoneBookDTO> getTop5PhoneBookContactsWithTheMostPhoneCalls() {
        Query query = entityManager.createQuery(QUERY);
        query.setMaxResults(5);
        return query.getResultList();
       // return phoneBookRepository.getPhoneBooksDtoOrderedByPhoneCallsCounterDESC().subList(0, 5);
    }


    /**
     * Commented code is left if phoneNumber have to be convert to phoneNumber list
     * @throws IOException
     */
    public void readTextFileAndWriteToDB() throws IOException {
        log.info("Reading file has started");
        Path path = Paths.get(phoneBookConfigurationProperties.getFile());

        Map<String, String> phoneBookMap = new HashMap<>();
//      Map<String, List<String>> phoneBookMapMultiNumber = new HashMap<>();

        try (BufferedReader reader = Files.newBufferedReader(path, ENCODING)) {
            String line;

            while ((line = reader.readLine()) != null) {
                String[] fieldsFromOneLine = line.split(",");
                String name = fieldsFromOneLine[0].trim().replaceFirst("\ufeff", "");
                String number = this.normalizePhoneNumber(fieldsFromOneLine[1].trim());

                if (this.validatePhoneNumber(number)) {
                   // phoneBookMapMultiNumber.computeIfAbsent(name, k -> new ArrayList<>()).add(number);
                    phoneBookMap.put(name,number);
                }
            }
            List<PhoneBook> phoneBookList = phoneBookMap.entrySet().stream()
                    .map(e -> new PhoneBook(e.getKey(), e.getValue()))
                    .collect(Collectors.toList());

//            List<PhoneBook> phoneBookList = phoneBookMapMultiNumber.entrySet().stream()
//                    .map(e -> new PhoneBook(e.getKey(), e.getValue()))
//                    .collect(Collectors.toList());


            log.info("Reading file has finished");
            this.saveToDB(phoneBookList);
        }

    }

    public void saveAll(List<PhoneBook> phoneBooks) {
        phoneBookRepository.saveAll(phoneBooks);
    }

    private Boolean validatePhoneNumber(String number) {
        return number.matches(PHONE_NUMBER_REGEX);
    }

    private String normalizePhoneNumber(String number) {
        return number.replaceFirst(PHONE_NUMBER_REGEX_CODE, PHONE_NUMBER_REGEX_CODE_REPLACEMENT);
    }

    private void saveToDB(List<PhoneBook> phoneBooks) {
        log.info("Started saving phone books from file to db");
        Stream<List<PhoneBook>> phoneBookStream = BatchUtil.splitToSublists(phoneBooks, BATCH_SIZE);

        phoneBookStream.forEach(phoneBook ->
                phoneBookRepository.saveAll(phoneBook)
        );

        log.info("Finished saving phone books from file to db");
    }
}
