package org.jobinterview.task.controller;


import org.jobinterview.task.model.PhoneBook;
import org.jobinterview.task.model.PhoneBookDTO;
import org.jobinterview.task.service.PhoneBookService;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PhoneBookController {

    private PhoneBookService phoneBookService;

    public PhoneBookController(PhoneBookService phoneBookService) {
        this.phoneBookService = phoneBookService;
    }

    @GetMapping("/contact/{name}")
    public PhoneBook get(@PathVariable(name = "name") String name) {
        return phoneBookService.findByName(name);
    }

    @GetMapping("/contacts")
    public Page<PhoneBook> getAll(
            @RequestParam(value = "page", defaultValue = "0") Integer page,
            @RequestParam(value = "limit", defaultValue = "20") Integer limit) {
        return phoneBookService.findAllOrderedByName(page, limit);
    }

    @PostMapping("/contact")
    public PhoneBook add(@Valid @RequestBody PhoneBook phoneBook) {
        return phoneBookService.addContact(phoneBook);
    }

    @DeleteMapping("/contact")
    public void delete(@RequestParam(name = "name") String name) {
        phoneBookService.deleteContactByName(name);
    }

    @PostMapping("/contact/{phoneNumber}/call")
    public void makePhoneCall(@PathVariable(name = "phoneNumber") String phoneNumber) {
        phoneBookService.makeAphoneCall(phoneNumber);
    }

    @GetMapping("/contacts/calls")
    public List<PhoneBookDTO> getTop5PhoneCalls() {
        return phoneBookService.getTop5PhoneBookContactsWithTheMostPhoneCalls();
    }

}
