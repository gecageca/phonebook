package org.jobinterview.task.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@Slf4j
@ControllerAdvice
public class PhoneBookExceptionController {


    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)  // 500
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ErrorMessage handleUnknown(Exception ex) {
        log.warn(ex.getMessage(), ex);
        ErrorMessage errMsg = new ErrorMessage();
        errMsg.setHttpStatusCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
        errMsg.setMessage(ex.getMessage());

        return errMsg;
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)  // 400
    @ExceptionHandler({IllegalArgumentException.class})
    @ResponseBody
    public ErrorMessage handleIllegalArgumentException(IllegalArgumentException ex) {
        log.trace(ex.getMessage());
        ErrorMessage errMsg = new ErrorMessage();
        errMsg.setHttpStatusCode(HttpStatus.BAD_REQUEST.value());
        errMsg.setMessage(ex.getMessage());
        return errMsg;
    }

    @ResponseStatus(HttpStatus.NOT_FOUND)  // 404
    @ExceptionHandler({PhoneBookNotFoundException.class})
    @ResponseBody
    public ErrorMessage handleNotFound(RuntimeException ex) {
        log.trace(ex.getMessage());
        ErrorMessage errMsg = new ErrorMessage();
        errMsg.setHttpStatusCode(HttpStatus.NOT_FOUND.value());
        errMsg.setMessage(ex.getMessage());
        return errMsg;
    }
}
