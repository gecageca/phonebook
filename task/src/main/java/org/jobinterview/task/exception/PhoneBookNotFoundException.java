package org.jobinterview.task.exception;

public class PhoneBookNotFoundException extends RuntimeException {


    public PhoneBookNotFoundException(String message) {
        super(message);
    }
}
