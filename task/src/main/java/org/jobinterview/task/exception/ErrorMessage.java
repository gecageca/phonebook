package org.jobinterview.task.exception;

import lombok.Getter;
import lombok.Setter;
import org.joda.time.DateTime;

@Getter
@Setter
public class ErrorMessage {

    Long timestamp;
    Integer HttpStatusCode;
    String message;

    public ErrorMessage() {
        this.timestamp = new DateTime().getMillis();
    }

    public ErrorMessage(int status, String message) {
        this.timestamp = new DateTime().getMillis();
        this.HttpStatusCode = status;
        this.message = message;
    }

}
