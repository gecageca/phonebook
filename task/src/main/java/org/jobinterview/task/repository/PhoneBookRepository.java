package org.jobinterview.task.repository;

import org.jobinterview.task.model.PhoneBook;
import org.jobinterview.task.model.PhoneBookDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PhoneBookRepository extends JpaRepository<PhoneBook, Long> {

    PhoneBook findByName(String name);

    Optional<PhoneBook> findByPhoneNumber(String phoneNumber);

    @Query("select new org.jobinterview.task.model.PhoneBookDTO(pb.id, pb.name, pb.phoneNumber, pb.phoneCallsCounter ) " +
            "FROM PhoneBook as pb  ORDER BY pb.phoneCallsCounter DESC ")
    List<PhoneBookDTO> getPhoneBooksDtoOrderedByPhoneCallsCounterDESC();
}
